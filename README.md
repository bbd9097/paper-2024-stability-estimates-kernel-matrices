## NOVEL AND REFINED STABILITY ESTIMATES FOR KERNEL MATRICES


This repository contains the supplementary material for the preprint
```
Novel and refined stability estimates for kernel matrices (2024)
T. Wenzel, A. ISKE
```

It was used to carry out the numerical experiments and generate the figures for the publication.
The experiments were performed on Linux systems in 2024 and were tested with a Python version that time (e.g., `python3.10`).


## Installation

After cloning the repository, execute the `setup.sh` file.
It will create a virtual environment and install the required tools.
To activate the virtual environment, use `source venv/bin/activate`



## Rerunning experiments and reproducing the plots

To rerun the numerical experiments of Section 4 and reproduce its plots, execute:

- `python3 main_01_spectral_alignment.py`
- `python3 main_02_optimality_estimates.py`


