# Code for the paper NOVEL AND REFINED STABILITY ESTIMATES FOR KERNEL MATRICES, see Subsection 5.1.
# It investigates the spectral alignment between eigenvectors of kernel matrices for kernels of different smoothness.



import numpy as np
from vkoga.kernels import Matern, Wendland, Gaussian
from matplotlib import pyplot as plt
from scipy.spatial import distance_matrix

np.random.seed(2)

# Print more output before line break
desired_width=400 # 320
np.set_printoptions(linewidth=desired_width)



# Variable settings
npoints = 20

dim = 1
k_mat_sigma = 0
k_mat_base = 2
kernel_sigma = Matern(k=k_mat_sigma)              # this is the \sigma kernel - we have \sigma < 1
kernel_base = Matern(k=k_mat_base)               # this is the base kernel

tau_sigma = (dim + (2*k_mat_sigma) + 1)/2
tau_base = (dim + (2*k_mat_base) + 1)/2                  # I use kernel_mat1 as base kernel!

sigma = tau_sigma / tau_base

dic_prefactors = {0: 2/np.sqrt(np.pi), 1: 2 * 2/np.sqrt(np.pi), 2: 8 * 2/np.sqrt(np.pi)}


X = np.random.rand(npoints, dim)
# X[-1, :] = X[0, :] + .0001

D0 = distance_matrix(X, X) + np.eye(X.shape[0]) * 100
q_sep = np.min(D0)


A0 = kernel_sigma.eval(X, X)
A1 = kernel_base.eval(X, X)

eig_val0, eig_vec0 = np.linalg.eig(A0)
eig_val1, eig_vec1 = np.linalg.eig(A1)

# sort eigenvalues and eigenvectors
idx_sort0 = np.argsort(eig_val0)[::-1]
idx_sort1 = np.argsort(eig_val1)[::-1]

eig_val0 = eig_val0[idx_sort0]
eig_val1 = eig_val1[idx_sort1]
eig_vec0 = eig_vec0[:, idx_sort0]
eig_vec1 = eig_vec1[:, idx_sort1]


array_dots_01 = np.abs(eig_vec0.T @ eig_vec1)
array_dots_10 = np.abs(eig_vec1.T @ eig_vec0)


## Visualize results
R = np.linspace(0, 1, int(1.5 * npoints))
array_color = plt.cm.hsv(R)



# heatmap plot of array_dots_10
plt.figure(3)
plt.clf()
plt.imshow(array_dots_10, cmap='hot', interpolation='nearest')
plt.colorbar()
plt.title('dim={}, q_sep={:.3e}'.format(dim, q_sep))
plt.show(block=False)










