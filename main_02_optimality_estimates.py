# Code for the paper NOVEL AND REFINED STABILITY ESTIMATES FOR KERNEL MATRICES, see Subsection 5.2.
# It investigates the optimality of the estimates of the main result Theorem 8.


import numpy as np
from vkoga.kernels import Matern, Wendland
from matplotlib import pyplot as plt


np.random.seed(2)

desired_width=400 # 320
np.set_printoptions(linewidth=desired_width)



# Variable settings
dim = 1
k_mat_sigma = 0
k_mat_base = 1              # TODO: Watch out, below we have hardcoded the choice of eps for the basic & linear Matern kernel!
kernel_sigma = Matern(k=k_mat_sigma)              # this is the \sigma kernel - we have \sigma < 1
kernel_base = Matern(k=k_mat_base)               # this is the base kernel

tau_sigma = (dim + (2*k_mat_sigma) + 1)/2
tau_base = (dim + (2*k_mat_base) + 1)/2                  # I use kernel_mat1 as base kernel!

sigma = tau_sigma / tau_base

dic_prefactors = {0: 2/np.sqrt(np.pi), 1: 2 * 2/np.sqrt(np.pi), 2: 8 * 2/np.sqrt(np.pi)}


n_points = 5
X = np.linspace(0, 1, n_points).reshape(-1, 1)


array_eps = np.geomspace(1e-2, 1e0, 100, endpoint=False).reshape(-1, 1)

array_eigvals_base = np.zeros((array_eps.shape[0], n_points))
array_eigvals_sigma = np.zeros((array_eps.shape[0], n_points))


n_quantities = 2
array_QOI = np.zeros((array_eps.shape[0], n_quantities))
array_upper_bound_QOI = np.zeros((array_eps.shape[0], n_quantities))
array_lower_bound_QOI_true = np.zeros((array_eps.shape[0], n_quantities))
array_lower_bound_QOI_false = np.zeros((array_eps.shape[0], n_quantities))




for idx_eps, eps in enumerate(array_eps):

    q_sep = eps * (1 / (n_points - 1))


    A_base = kernel_base.eval(eps*X, eps*X) / dic_prefactors[k_mat_base]
    A_sigma = kernel_sigma.eval(eps*X, eps*X) / dic_prefactors[k_mat_sigma]         # divide by prefactors to have Fourier transform exactly (1+||omega||^2)^{-\tau}

    # Compute eigenvectors and eigenvalues
    eig_val0, eig_vec0 = np.linalg.eig(A_base)
    eig_val1, eig_vec1 = np.linalg.eig(A_sigma)

    # sort eigenvalues and eigenvectors
    idx_sort0 = np.argsort(eig_val0)[::-1]
    eig_val0 = eig_val0[idx_sort0]
    eig_vec0 = eig_vec0[:, idx_sort0]

    # sort eigenvalues and eigenvectors
    idx_sort1 = np.argsort(eig_val1)[::-1]
    eig_val1 = eig_val1[idx_sort1]
    eig_vec1 = eig_vec1[:, idx_sort1]

    array_eigvals_base[idx_eps, :] = eig_val0[:n_points]
    array_eigvals_sigma[idx_eps, :] = eig_val1[:n_points]


    # Computation of quantities of interest: QOI1 that realizes lower bound, QOI2 that realizes upper bound
    weight1 = eps ** .5
    weight2 = np.sqrt(1 - weight1**2)

    alpha_special = weight1 * eig_vec0[:, [0]] + weight2 * eig_vec0[:, [-1]]
    # alpha_special = eig_vec0[:, [-1]]

    
    list_alpha = [alpha_special, eig_vec0[:, [-1]]]

    assert len(list_alpha) == n_quantities, 'Modify n_quantities'


    for idx_alpha, alpha in enumerate(list_alpha):
        # Compute quantities
        QOI = np.sum(alpha * (A_sigma @ alpha), axis=0) / np.sum(alpha * alpha, axis=0)

        # For upper bound, a factor of three is added 
        upper_bound_QOI = 3 * q_sep ** (-dim * (1-sigma)) * (np.sum(alpha * (A_base @ alpha), axis=0) / np.sum(alpha * alpha, axis=0)) ** sigma
        lower_bound_QOI_true = (np.sum(alpha * (A_base @ alpha), axis=0) / np.sum(alpha * alpha, axis=0))
        lower_bound_QOI_false = (np.sum(alpha * (A_base @ alpha), axis=0) / np.sum(alpha * alpha, axis=0)) ** sigma


        # Store quantities
        array_QOI[idx_eps, idx_alpha] = np.real(QOI).item()

        array_upper_bound_QOI[idx_eps, idx_alpha] = upper_bound_QOI.item()
        array_lower_bound_QOI_true[idx_eps, idx_alpha] = lower_bound_QOI_true.item()
        array_lower_bound_QOI_false[idx_eps, idx_alpha] = lower_bound_QOI_false.item()





## Some plots
plt.figure(1)
plt.clf()
for idx_point in range(n_points):
    plt.plot(array_eps, array_eigvals_base[:, idx_point], 'b-', label='base, point {}'.format(idx_point))
    plt.plot(array_eps, array_eigvals_sigma[:, idx_point], 'r--', label='sigma, point {}'.format(idx_point))
plt.legend()
plt.grid(True)
plt.xscale('log')
plt.yscale('log')
plt.show(block=False)


for idx_alpha, _ in enumerate(list_alpha):
    plt.figure(10 + idx_alpha)
    plt.clf()
    plt.plot(array_eps, array_upper_bound_QOI[:, idx_alpha], 'b--', label='upper bound')
    plt.plot(array_eps, array_QOI[:, idx_alpha], 'r-', label='QOI1')
    plt.plot(array_eps, array_lower_bound_QOI_true[:, idx_alpha], 'g--', label='lower bound true')
    plt.plot(array_eps, array_lower_bound_QOI_false[:, idx_alpha], 'm-.', label='lower bound false')
    # plt.plot(array_eps, array_eigvals_sigma, 'k--')
    plt.legend()
    plt.grid(True)
    plt.xscale('log')
    plt.yscale('log')
    plt.title('idx_alpha = {}'.format(idx_alpha))
    plt.show(block=False)



























# kappa = sigma * tau_base            # this is essentially tau_sigma

# if sigma <= dim / (2*tau_base):
#     for _ in range(10):
#         print('sigma is too small!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')


# # Create data points
# n_largest = 4
# n_smallest = 4

# list_n_points = [2**i for i in range(3, 10)]

# array_QOI = np.zeros((n_largest + n_smallest + 1, len(list_n_points)))
# array_upper_bound_QOI = np.zeros((n_largest + n_smallest + 1, len(list_n_points)))          # +1, because we want to investigate one special hand-made alpha vector
# array_lower_bound_QOI = np.zeros((n_largest + n_smallest + 1, len(list_n_points)))          # +1, because we want to investigate one special hand-made alpha vector


# # I also want to store some weight_functions
# max_omega = 500
# omega = np.linspace(-.2*max_omega, 1.8*max_omega, 20000).reshape(-1, 1)
# list_weight_func_A0 = []
# list_weight_func_A1 = []
# list_weight_func_special = []



# for idx_points, n_points in enumerate(list_n_points):
#     # Scale dataset, compute new seperation distance
#     q_sep = 1 / (n_points - 1)


#     X = np.linspace(0, 1, n_points).reshape(-1, 1)
#     X += .05 * q_sep * np.random.randn(*X.shape)    # add some noise to the points

    
    
#     A_sigma = kernel_sigma.eval(X, X)
#     A_base = kernel_base.eval(X, X)

#     # Compute eigenvectors and eigenvalues
#     eig_val0, eig_vec0 = np.linalg.eig(A_base)
#     eig_val1, eig_vec1 = np.linalg.eig(A_sigma)

#     # sort eigenvalues and eigenvectors
#     idx_sort0 = np.argsort(eig_val0)[::-1]
#     eig_val0 = eig_val0[idx_sort0]
#     eig_vec0 = eig_vec0[:, idx_sort0]

#     # sort eigenvalues and eigenvectors
#     idx_sort1 = np.argsort(eig_val1)[::-1]
#     eig_val1 = eig_val1[idx_sort1]
#     eig_vec1 = eig_vec1[:, idx_sort1]

#     # choose an alpha vector
#     # alpha = eig_vec0[:, [idx_alpha]]
#     alpha = eig_vec0


#     # Compute the QOI as well as the upper bound on the QOI
#     QOI = np.sum(alpha * (A_sigma @ alpha), axis=0) / np.sum(alpha * alpha, axis=0)              # mind that parantheses are important!
#     upper_bound_QOI = q_sep ** (-dim * (1-sigma)) * np.sum(alpha * (A_base @ alpha), axis=0) ** sigma    # mind that parantheses are important!
#     lower_bound_QOI = (np.sum(alpha * (A_base @ alpha), axis=0) / np.sum(alpha * alpha, axis=0)) # ** sigma    # mind that parantheses are important!
#     # upper_bound_QOI = (alpha.T @ A_base @ alpha)

#     array_QOI[:n_smallest, idx_points] = QOI[:n_smallest]
#     array_QOI[n_smallest:n_smallest + n_largest, idx_points] = QOI[-n_largest:]

#     array_upper_bound_QOI[:n_smallest, idx_points] = upper_bound_QOI[:n_smallest]
#     array_upper_bound_QOI[n_smallest:n_smallest + n_largest, idx_points] = upper_bound_QOI[-n_largest:]

#     array_lower_bound_QOI[:n_smallest, idx_points] = lower_bound_QOI[:n_smallest]
#     array_lower_bound_QOI[n_smallest:n_smallest + n_largest, idx_points] = lower_bound_QOI[-n_largest:]

#     # Hand crafted value for alpha
#     # alpha_special = np.mean(alpha, axis=1).reshape(-1, 1)

#     # alpha_special = np.ones((alpha.shape[0], 1))
#     # alpha_special = alpha_special / np.linalg.norm(alpha_special)

#     # alpha_special = np.zeros((alpha.shape[0], 1))
#     # alpha_special[0] = 1

#     # Construction of an adversary alpha vector according to 24.06.24\C
#     # weight1 = 1
#     # weight2 = (1/q_sep)**(2*kappa) * weight1          # \C2

#     # # weight1 = 1
#     # weight2 = (1/q_sep) ** (tau_base / np.sqrt(dim))    # \C3
#     # sum_weight = weight1 + weight2
#     # weight1 = weight1 / sum_weight
#     # weight2 = weight2 / sum_weight

#     # \C8
#     weight1 = n_points ** (-.75)
#     weight1 = n_points ** (-1.)
#     weight2 = np.sqrt(1 - weight1**2)


#     alpha_special = weight1 * alpha[:, [0]] + weight2 * alpha[:, [-1]]

#     print('n_points = {}: A:                first value = {:.3e}, second value = {:.3e}'.format(n_points, np.real(weight1**2 * eig_val0[0]), np.real(weight2**2 * eig_val0[-1])))
#     print('n_points = {}: A_sigma fake:     first value = {:.3e}, second value = {:.3e}'.format(n_points, np.real(weight1**2 * eig_val1[0]), np.real(weight2**2 * eig_val1[-1])))
#     print('n_points = {}: A_sigma true:     first value = {:.3e}, second value = {:.3e}'.format(n_points, np.real(weight1**2 * eig_vec0[:, [0]].T @ A_sigma @ eig_vec0[:, [0]]).item(), 
#                                                                                                 np.real(weight2**2 * eig_vec0[:, [-1]].T @ A_sigma @ eig_vec0[:, [-1]]).item()))
#     print(' ')


#     QOI_special = np.sum(alpha_special * (A_sigma @ alpha_special), axis=0) / np.sum(alpha_special * alpha_special, axis=0)
#     upper_bound_QOI_special = q_sep ** (-dim * (1-sigma)) * np.sum(alpha_special * (A_base @ alpha_special), axis=0) ** sigma
#     lower_bound_QOI_special = (np.sum(alpha_special * (A_base @ alpha_special), axis=0) / np.sum(alpha_special * alpha_special, axis=0)) # ** sigma

#     array_QOI[-1, idx_points] = np.real(QOI_special)
#     array_upper_bound_QOI[-1, idx_points] = upper_bound_QOI_special
#     array_lower_bound_QOI[-1, idx_points] = lower_bound_QOI_special


#     # Visualize the weight function for the smallest eigenvector
#     weight_func = lambda omega, alpha, X: np.abs(np.sum(alpha.T * np.exp(1j * omega * X.T), axis=1))**2

#     if dim == 1:            # only makes sense for dim==1!!!!
#         list_weight_func_A0.append(weight_func(omega, eig_vec0[:, [-1]], X))
#         list_weight_func_A1.append(weight_func(omega, eig_vec1[:, [-1]], X))
#         list_weight_func_special.append(weight_func(omega, alpha_special, X))






# ## Some plots
# R = np.linspace(0, 1, int(1.5 * (n_smallest + n_largest)))
# array_color = plt.cm.hsv(R)

# # Plot QOI and upper estimate
# plt.figure(10)
# plt.clf()
# for idx_eigval in range(n_smallest + n_largest):
#     plt.plot(list_n_points, array_upper_bound_QOI[idx_eigval, :], '.-', color=array_color[idx_eigval, :], label='upper bound QOI, idx_eigval={}'.format(idx_eigval))
# for idx_eigval in range(n_smallest + n_largest):
#     plt.plot(list_n_points, array_QOI[idx_eigval, :], 'x--', color=array_color[idx_eigval, :], label='QOI, idx_eigval={}'.format(idx_eigval))

# plt.plot(list_n_points, array_upper_bound_QOI[-1, :], 'k.-', label='upper bound QOI special')
# plt.plot(list_n_points, array_QOI[-1, :], 'kx--', label='QOI special')

# plt.legend()
# plt.xscale('log')
# plt.yscale('log')
# plt.xlabel('number of points')
# plt.title('QOI and upper bound')
# plt.show(block=False)


# # Plot ratio of upper estimate divided by QOI
# plt.figure(11)
# plt.clf()
# for idx_eigval in range(n_smallest + n_largest):
#     plt.plot(list_n_points, array_upper_bound_QOI[idx_eigval, :] / array_QOI[idx_eigval, :], '.-', color=array_color[idx_eigval, :], label='upper bound / QOI, idx_eigval={}'.format(idx_eigval))
# plt.plot(list_n_points, array_upper_bound_QOI[-1, :] / array_QOI[-1, :], 'k.-', label='upper bound / QOI, special')
# plt.legend()
# plt.xscale('log')
# # plt.yscale('log')
# plt.xlabel('number of points')
# plt.title('upper bound / QOI')
# plt.show(block=False)




# # Plot QOI and lower estimate
# plt.figure(20)
# plt.clf()
# for idx_eigval in range(n_smallest + n_largest):
#     plt.plot(list_n_points, array_lower_bound_QOI[idx_eigval, :], '.-', color=array_color[idx_eigval, :], label='lower bound QOI, idx_eigval={}'.format(idx_eigval))
# for idx_eigval in range(n_smallest + n_largest):
#     plt.plot(list_n_points, array_QOI[idx_eigval, :], 'x--', color=array_color[idx_eigval, :], label='QOI, idx_eigval={}'.format(idx_eigval))

# plt.plot(list_n_points, array_lower_bound_QOI[-1, :], 'k.-', label='lower bound QOI special')
# plt.plot(list_n_points, array_QOI[-1, :], 'kx--', label='QOI special')

# plt.legend()
# plt.xscale('log')
# plt.yscale('log')
# plt.xlabel('number of points')
# plt.title('QOI and lower bound')
# plt.show(block=False)


# # Plot ratio of QOI divided by lower estimate
# plt.figure(21)
# plt.clf()
# for idx_eigval in range(n_smallest + n_largest):
#     plt.plot(list_n_points, array_QOI[idx_eigval, :] / array_lower_bound_QOI[idx_eigval, :], '.-', color=array_color[idx_eigval, :], label='QOI / upper bound, idx_eigval={}'.format(idx_eigval))
# plt.plot(list_n_points, array_QOI[-1, :] / array_lower_bound_QOI[-1, :], 'k.-', label='QOI / lower bound, special')
# plt.plot(list_n_points, 1/np.array(list_n_points)**(2*sigma), 'k.--', label='n^{-2sigma}')
# plt.legend()
# plt.xscale('log')
# plt.yscale('log')
# plt.xlabel('number of points')
# plt.title('QOI / lower bound')
# plt.show(block=False)



# # Visualize weight functions for alpha special vector

# # Visualize some weight functions
# for idx_points, n_points in enumerate(list_n_points):

#     continue

#     plt.figure(10000 + idx_points)
#     plt.clf()
#     plt.plot(omega, list_weight_func_special[idx_points], 'b-')
#     plt.plot(omega, list_weight_func_special[idx_points], 'r--')
#     plt.title('npoints = {}'.format(n_points))
#     plt.show(block=False)



